import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ErrorFieldComponent } from './error-field/error-field.component';
import { TextFieldComponent } from './text-field/text-field.component';
import { FieldLabelComponent } from './field-label/field-label.component';
import { PhoneFieldComponent } from './phone-field/phone-field.component';
import { PipesModule } from '../../pipes/pipes.module';
import { DropdownItemComponent } from './phone-field/dropdown-item/dropdown-item.component';
import { FlagIconComponent } from './phone-field/flag-icon/flag-icon.component';

@NgModule({
  declarations: [
    ErrorFieldComponent,
    TextFieldComponent,
    FieldLabelComponent,
    PhoneFieldComponent,
    DropdownItemComponent,
    FlagIconComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PipesModule
  ],
  exports: [
    ErrorFieldComponent,
    TextFieldComponent,
    FieldLabelComponent,
    PhoneFieldComponent
  ]
})
export class ControlsModule {
}
