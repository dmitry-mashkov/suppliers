import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-text-field',
  templateUrl: './text-field.component.html',
  styleUrls: ['./text-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextFieldComponent),
      multi: true
    }
  ]
})
export class TextFieldComponent implements OnInit, ControlValueAccessor {
  @Input() name: string;
  @Input() placeholder: string;
  @Input() isValid: boolean;
  @Input() required: boolean;
  @Input() errorMsg: string;

  text: string;
  private data: any;

  constructor() { }

  ngOnInit() {
  }

  onChange(event) {
    this.data = event.target.value;

    // update the form
    this.propagateChange(this.data);
  }

  writeValue(value: any) {
    if (value !== undefined) {
      this.text = value;
    }
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  propagateChange = (_: any) => { };

  registerOnTouched() { }


}
