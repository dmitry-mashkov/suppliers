/**
 * @overview    Enumeration of control types
 */

export enum ControlTypes {
  Text = 0,
  Phone = 1,
  Tags = 2
}
