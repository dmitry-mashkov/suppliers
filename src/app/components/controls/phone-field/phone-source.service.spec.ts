import { TestBed, inject } from '@angular/core/testing';

import { PhoneSourceService } from './phone-source.service';

describe('PhoneSourceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PhoneSourceService]
    });
  });

  it('should be created', inject([PhoneSourceService], (service: PhoneSourceService) => {
    expect(service).toBeTruthy();
  }));
});
