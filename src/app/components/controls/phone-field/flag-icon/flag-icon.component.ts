import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-flag-icon',
  templateUrl: './flag-icon.component.html',
  styleUrls: ['./flag-icon.component.scss']
})
export class FlagIconComponent implements OnInit {
  @Input() source: string;
  @Input() country: string;

  constructor() { }

  ngOnInit() {
  }

}
