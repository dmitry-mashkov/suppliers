import { Injectable } from '@angular/core';
import { CountryCode } from './country-code';

@Injectable({
  providedIn: 'root'
})
export class PhoneSourceService {
  private countryCodes: CountryCode[];

  constructor() {
    this.fillCodes();
  }

  getCountryCodes(): CountryCode[] {
    return this.countryCodes;
  }

  private fillCodes(): void {
    this.countryCodes = [
      {
        flagImage: 'au.svg',
        countryCode: 'AU',
        countryFull: 'Australia',
        phoneCode: '+21'
      },
      {
        flagImage: 'jp.svg',
        countryCode: 'JP',
        countryFull: 'Japan',
        phoneCode: '+81'
      },
      {
        flagImage: 'es.svg',
        countryCode: 'ES',
        countryFull: 'Spain',
        phoneCode: '+22'
      },
      {
        flagImage: 'gb.svg',
        countryCode: 'GB',
        countryFull: 'United Kingdom',
        phoneCode: '+44'
      },
      {
        flagImage: 'za.svg',
        countryCode: 'ZA',
        countryFull: 'South Africa',
        phoneCode: '+27'
      },
      {
        flagImage: 'zm.svg',
        countryCode: 'ZM',
        countryFull: 'Zambia',
        phoneCode: '+23'
      }
    ];
  }
}
