import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CountryCode } from '../country-code';

@Component({
  selector: 'app-dropdown-item',
  templateUrl: './dropdown-item.component.html',
  styleUrls: ['./dropdown-item.component.scss']
})
export class DropdownItemComponent implements OnInit {
  @Input() countryCode: CountryCode;
  @Input() isActive: boolean;
  @Output() itemSelect = new EventEmitter<CountryCode>();

  constructor() { }

  ngOnInit() {
  }

  onItemClick() {
    this.itemSelect.emit(this.countryCode);
  }

}
