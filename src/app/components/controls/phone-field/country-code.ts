export class CountryCode {
  flagImage: string;
  countryCode: string;
  countryFull: string;
  phoneCode: string;
}
