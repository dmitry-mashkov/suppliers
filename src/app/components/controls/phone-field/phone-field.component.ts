import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CountryCode } from './country-code';
import { PhoneSourceService } from './phone-source.service';
import { FilterPipe } from '../../../pipes/filter.pipe';

@Component({
  selector: 'app-phone-field',
  templateUrl: './phone-field.component.html',
  styleUrls: ['./phone-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PhoneFieldComponent),
      multi: true
    }
  ]
})
export class PhoneFieldComponent implements OnInit, ControlValueAccessor {
  @Input() name: string;
  @Input() placeholder: string;
  @Input() isValid: boolean;
  @Input() required: boolean;
  @Input() errorMsg: string;

  number: string;
  private data: any;
  protected countryCodes: CountryCode[];
  protected countryCode: CountryCode;
  protected filter = '';
  protected isDropdownExpanded = false;

  constructor(
    private phoneSourceService: PhoneSourceService,
    private filterPipe: FilterPipe
  ) { }

  get filteredCountryCodes() {
    return this.filterPipe.transform(this.countryCodes, 'countryFull', this.filter);
  }

  ngOnInit() {
    this.countryCodes = this.phoneSourceService.getCountryCodes();

    this.countryCode = this.countryCodes.length ? this.countryCodes[0] : new CountryCode();
  }

  onChange(event) {
    this.data = event.target.value;

    // update the form
    this.propagateChange(this.data);
  }

  writeValue(value: any) {
    if (value !== undefined) {
      this.number = value;
    }
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  propagateChange = (_: any) => { };

  registerOnTouched() { }

  onExpanderClick() {
    this.isDropdownExpanded = !this.isDropdownExpanded;
  }

  onCountrySelect(countryCode: CountryCode) {
    this.countryCode = countryCode;
    this.isDropdownExpanded = false;
  }
}
