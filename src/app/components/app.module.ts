import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { EditSupplierModule} from './edit/edit-supplier.module';
import { AppComponent } from './app.component';
import { SuppliersListModule } from './suppliers-list/suppliers-list.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    EditSupplierModule,
    FontAwesomeModule,
    SuppliersListModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
