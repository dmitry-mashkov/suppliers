import { ValidatorFn } from '@angular/forms/src/directives/validators';
import { ControlTypes } from '../controls/control-types.enumeration';

export interface FormField {
  name: string;
  label: string;
  placeholder: string;
  type: ControlTypes;
  validator: ValidatorFn | ValidatorFn[];
}
