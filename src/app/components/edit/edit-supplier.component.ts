/**
 * @overview    Edit supplier component
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as _ from 'lodash';
import { map, get, getOr } from 'lodash/fp';

import { FormModes } from './form-modes.enumeration';
import { Supplier } from '../../model/supplier';
import { GroupsService } from '../../service/groups.service';
import { SupplierFieldsConfig } from './supplier-fields.config';
import { FormField } from './form-field.interface';
import { ControlTypes } from '../controls/control-types.enumeration';
import { TagModelClass } from 'ngx-chips/core/accessor';

@Component({
  selector: 'app-edit-supplier',
  templateUrl: './edit-supplier.component.html',
  styleUrls: ['./edit-supplier.component.scss']
})
export class EditSupplierComponent implements OnInit {
  @Input() mode: FormModes;
  @Input() supplier: Supplier;
  @Output() supplierEdit = new EventEmitter<Supplier>();
  model: Supplier;
  title: string;
  groups: string[];
  form: FormGroup;
  CONTROL_TYPES;
  config: FormField[];

  constructor(
    public activeModal: NgbActiveModal,
    private groupsService: GroupsService,
    private formBuilder: FormBuilder
  ) {
    this.CONTROL_TYPES = ControlTypes;
    this.config = SupplierFieldsConfig;
  }


  ngOnInit() {
    this.model = (this.mode === FormModes.Add) ? new Supplier() : _.cloneDeep(this.supplier);

    if (this.mode === FormModes.Add) {
      this.title = 'Add new supplier';

    } else if (this.mode === FormModes.Edit) {
      this.title = `Edit "${this.model.name}"`;

    }
    this.groups = this.groupsService.groups;

    this.createForm();
  }

  createForm() {
    this.form = this.formBuilder.group(_.reduce(
      SupplierFieldsConfig,
      (result, fieldConfig: FormField) => _.assign(result, {
        [fieldConfig.name]: [null, fieldConfig.validator]
      }),
      {}
    ));

    this.form.setValue(_.assign(
      {
        name: '',
        address: '',
        email: '',
        phone: '',
        groups: []
      },
      _.omit(this.model, 'groups'),
      { groups: map(g => ({ value: g, display: g }))(this.model.groups) }
    ));
  }

  isInvalid(name: string) {
    const field = this.form.get(name);

    return field.invalid && (field.dirty || field.touched);
  }

  getFieldErrorMsg(name: string) {
    const field = this.form.get(name),
          errors = field.errors,
          label = _.upperFirst(name);

    if (get('required')(errors)) return `"${label}" field is required`;
    if (get('minlength')(errors)) return `${label} should be at least ${errors.minlength.requiredLength} symbols`;
    if (get('maxlength')(errors)) return `${label} should be at most ${errors.maxlength.requiredLength} symbols`;

    return '';
  }

  save() {
    if (!this.form.valid) {
      this.validateFields();

    } else {
      const supplier: Supplier = _.assign(
        {},
        _.omit(this.form.value, 'groups'),
        { groups: map(g => g.value)(this.form.value.groups)}
      );

      this.supplierEdit.emit(supplier);
      this.activeModal.close('Save');
    }
  }

  onGroupAdd(group: TagModelClass) {
    this.groupsService.addGroup(group.value);
  }

  onGroupRemove(group: TagModelClass) {
    this.groupsService.removeGroup(group.value);
  }

  validateFields() {
    this.config.forEach(({ name }) => {
      this.form.get(name).markAsTouched({ onlySelf: true });
    });
  }
}
