/**
 * @overview    Enumeration of form modes
 */

export enum FormModes {
  Add = 0,
  Edit = 1
}
