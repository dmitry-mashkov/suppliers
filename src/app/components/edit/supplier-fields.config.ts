/**
 * @overview    Description of supplier form fields
 */
import { ControlTypes } from '../controls/control-types.enumeration';
import { FormField } from './form-field.interface';
import { Validators } from '@angular/forms';

export const SupplierFieldsConfig: FormField[] = [
  {
    name: 'name',
    label: 'Name',
    placeholder: 'John Doe',
    type: ControlTypes.Text,
    validator: Validators.compose([
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(50)
    ])
  },
  {
    name: 'address',
    label: 'Address',
    placeholder: 'Abbey Road',
    type: ControlTypes.Text,
    validator: Validators.required
  },
  {
    name: 'email',
    label: 'Email',
    placeholder: 'john.doe@example.com',
    type: ControlTypes.Text,
    validator: Validators.required
  },
  {
    name: 'phone',
    label: 'Phone',
    placeholder: '0000 000 0000',
    type: ControlTypes.Phone,
    validator: Validators.required
  },
  {
    name: 'groups',
    label: 'Groups',
    placeholder: 'Office Supplies',
    type: ControlTypes.Tags,
    validator: Validators.required
  }
];
