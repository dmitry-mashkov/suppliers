import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TagInputModule } from 'ngx-chips';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { EditSupplierComponent } from './edit-supplier.component';
import { ControlsModule } from '../controls/controls.module';

@NgModule({
  declarations: [
    EditSupplierComponent
  ],
  imports: [
    NgbModule.forRoot(),
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    TagInputModule,
    BrowserAnimationsModule,
    ControlsModule
  ],
  entryComponents: [
    EditSupplierComponent
  ]
})
export class EditSupplierModule {
}
