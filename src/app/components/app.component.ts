import { Component } from '@angular/core';
import {
  faListUl,
  faSitemap,
  faPlusCircle
} from '@fortawesome/free-solid-svg-icons';

import { SuppliersService } from '../service/suppliers.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Office suppliers';
  filterPhrase = '';
  isGrouped = false;
  faListUl = faListUl;
  faSitemap = faSitemap;
  faPlusCircle = faPlusCircle;

  constructor(private suppliersService: SuppliersService) {
  }

  onAddSupplierClick() {
    this.suppliersService.addSupplier();
  }

  onFlatViewClick() {
    this.isGrouped = false;
  }

  onGroupedViewClick() {
    this.isGrouped = true;
  }
}
