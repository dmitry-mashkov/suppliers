/**
 * @overview    Suppliers list module
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { SuppliersListComponent } from './suppliers-list.component';
import { SupplierItemComponent } from './supplier-item/supplier-item.component';
import { GroupListComponent } from './group-list/group-list.component';
import { GroupNamePipe } from './group-name/group-name.pipe';
import { GroupItemComponent } from './group-item/group-item.component';

@NgModule({
  declarations: [
    SuppliersListComponent,
    SupplierItemComponent,
    GroupListComponent,
    GroupNamePipe,
    GroupItemComponent
  ],
  exports: [
    SuppliersListComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  providers: [
    GroupNamePipe
  ]
})
export class SuppliersListModule { }
