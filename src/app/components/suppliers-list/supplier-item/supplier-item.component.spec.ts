import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierItemComponent } from './supplier-item.component';
import { Supplier } from '../../../model/supplier';
import {Component} from '@angular/core';

@Component({
  selector: 'app-host-component',
  template: `<app-supplier-item [supplier]="supplier"></app-supplier-item>`
})
class TestHostComponent {
  supplier: Supplier;

  constructor() {
    this.supplier = {
      name: 'Test supplier',
      address: '',
      email: '',
      phone: '',
      groups: ['']
    };
  }
}

xdescribe('SupplierItemComponent', () => {
  let testHostComponent: TestHostComponent;
  let testHostFixture: ComponentFixture<TestHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierItemComponent, TestHostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    testHostFixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = testHostFixture.componentInstance;
    testHostFixture.detectChanges();
  });

  it('should create', () => {
    // expect(component).toBeTruthy();
  });
});


