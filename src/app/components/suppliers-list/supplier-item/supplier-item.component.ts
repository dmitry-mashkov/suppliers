import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faPencilAlt, faTimes } from '@fortawesome/free-solid-svg-icons';

import { Supplier } from '../../../model/supplier';

@Component({
  selector: 'app-supplier-item',
  templateUrl: './supplier-item.component.html',
  styleUrls: ['./supplier-item.component.scss']
})
export class SupplierItemComponent implements OnInit {

  @Input() showGroups: boolean;
  @Input() supplier: Supplier;
  @Output() supplierDelete = new EventEmitter<Supplier>();
  @Output() supplierEdit = new EventEmitter<Supplier>();

  faPencilAlt = faPencilAlt;
  faTimes = faTimes;

  constructor() {
  }

  ngOnInit() {
  }

  onEditClick(supplier: Supplier) {
    this.supplierEdit.emit(supplier);
  }

  onDeleteClick(supplier: Supplier) {
    this.supplierDelete.emit(supplier);
  }

}
