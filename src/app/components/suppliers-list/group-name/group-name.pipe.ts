import { Pipe, PipeTransform } from '@angular/core';
import { toLower, filter } from 'lodash';

import { Supplier } from '../../../model/supplier';

@Pipe({
  name: 'groupName'
})
/**
 * @deprecated
 * @description  FilterPipe should be used
 */
export class GroupNamePipe implements PipeTransform {

  transform(suppliers: Supplier[], groupName: string): Supplier[] {
    return filter(suppliers, s => (toLower(s.name).indexOf(toLower(groupName)) > -1));
  }

}
