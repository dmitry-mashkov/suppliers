import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html'
})
export class GroupListComponent implements OnInit {

  @Input() groups: string[];

  constructor() { }

  ngOnInit() {
  }

}
