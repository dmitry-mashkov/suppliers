import { Component, Input, OnInit } from '@angular/core';

import { Supplier } from '../../model/supplier';
import { SuppliersService } from '../../service/suppliers.service';
import { GroupNamePipe } from './group-name/group-name.pipe';
import { GroupsService } from '../../service/groups.service';

@Component({
  selector: 'app-suppliers-list',
  templateUrl: './suppliers-list.component.html'
})
export class SuppliersListComponent implements OnInit {

  @Input() isGrouped = false;
  @Input() filterPhrase = '';
  groups: string[];


  constructor(
    private suppliersService: SuppliersService,
    private groupsService: GroupsService,
    private groupNamePipe: GroupNamePipe
  ) {
    this.groups = this.groupsService.groups;
  }

  get suppliers(): Supplier[] {
    return this.groupNamePipe.transform(this.suppliersService.suppliers, this.filterPhrase);
  }

  get showGrouped(): boolean {
    return this.suppliers.length && this.isGrouped;
  }

  get showListed(): boolean {
    return this.suppliers.length && !this.isGrouped;
  }

  get showEmpty(): boolean {
    return !this.suppliers.length;
  }

  ngOnInit() {
  }

  onSupplierEdit(supplier: Supplier) {
    this.suppliersService.editSupplier(supplier);
  }

  getGroupSuppliers(group: string) {
    return this.suppliers.filter(s => s.groups.includes(group));
  }

  getNotEmptyGroups() {
    return this.groups.filter(g => this.suppliers.some(s => s.groups.includes(g)));
  }

  onSupplierDelete(supplier: Supplier) {
    // TODO: raise confirmation popup window
    this.suppliersService.deleteSupplier(supplier);
  }

}

