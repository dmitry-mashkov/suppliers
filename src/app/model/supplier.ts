/**
 * @overview    Supplier model
 */

export class Supplier {
  name: string;
  address: string;
  email: string;
  phone: string;
  groups: string[];
}
