/**
 * @overview    Suppliers service unit testing
 */

import { TestBed } from '@angular/core/testing';

import { SuppliersService } from './suppliers.service';
import { SUPPLIERS } from '../mock/suppliers.mock';

describe('SuppliersService', () => {
  let service: SuppliersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SuppliersService]
    });

    service = new SuppliersService();
  });

  afterAll(() => {
    service = null;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('gets suppliers from the mock', () => {
    expect(service.suppliers).toBe(SUPPLIERS);
  });

  it('deletes a supplier from the list', () => {
    const previousCount = SUPPLIERS.length;
    service.deleteSupplier(SUPPLIERS[0]);

    expect(service.suppliers.length).toBe(previousCount - 1);
  });
});
