import { Injectable } from '@angular/core';
import { pull } from 'lodash';
import { GROUPS } from '../mock/groups.mock';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {
  groups: string[];

  constructor() {
    this.groups = GROUPS;
  }

  addGroup(group: string) {
    this.groups.push(group);
  }

  removeGroup(group: string) {
    pull(this.groups, group);
  }
}
