/**
 * @overview    Suppliers service to read and modify their data
 */

import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { pull, assign } from 'lodash';

import { SUPPLIERS } from '../mock/suppliers.mock';
import { Supplier } from '../model/supplier';
import { EditSupplierComponent } from '../components/edit/edit-supplier.component';
import { FormModes } from '../components/edit/form-modes.enumeration';

@Injectable({
  providedIn: 'root'
})
export class SuppliersService {
  suppliers: Supplier[];

  constructor(private modalService: NgbModal) {
    this.suppliers = SUPPLIERS;
  }


  public deleteSupplier(supplier: Supplier) {
    pull(this.suppliers, supplier);
  }

  public editSupplier(supplier: Supplier) {
    const modal = this.modalService.open(EditSupplierComponent);
    modal.componentInstance.mode = FormModes.Edit;
    modal.componentInstance.supplier = supplier;
    modal.componentInstance.supplierEdit.subscribe(s => this.onSupplierEdit(s, supplier));
  }

  public addSupplier() {
    const modal = this.modalService.open(EditSupplierComponent);
    modal.componentInstance.mode = FormModes.Add;
    modal.componentInstance.supplierEdit.subscribe(s => this.onSupplierAdd(s));
  }

  private onSupplierEdit(newSupplier: Supplier, oldSupplier: Supplier) {
    assign(oldSupplier, newSupplier);
  }

  private onSupplierAdd(newSupplier: Supplier) {
    this.suppliers.push(newSupplier);
  }

}
