/**
 * @overview    groups.mock
 */

const groups = ['Office Supplies', 'Office Equipment', 'Stationery', 'Cleaners'];

export const GROUPS: string[] = groups;
// export const GROUPS = groups.reduce((result, group) => Object.assign(result, {[group]: group}), {});
// export const GROUPS: Group[] = [
//   { name: 'Office Supplies' },
//   { name: 'Office Equipment' },
//   { name: 'Stationery' },
//   { name: 'Cleaners' }
// ];
