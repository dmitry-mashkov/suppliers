/**
 * @overview    Dummy suppliers list
 */

import { Supplier } from '../model/supplier';

export const SUPPLIERS: Supplier[] = [
  {
    name: 'OfficeXpress',
    address: 'Roydsdale Way, Enterprise House',
    email: 'sales@officexpress.co.uk',
    phone: '0845 890 1995',
    groups: ['Office Supplies', 'Office Equipment']
  },
  {
    name: 'DC Direct',
    address: '34 Threadneedle Street',
    email: 'hello@dcdirect.london',
    phone: '0800 988 3138',
    groups: ['Office Supplies', 'Stationery']
  },
  {
    name: 'Red Office',
    address: 'Oxgate Lane',
    email: 'sales@redoffice22.co.uk',
    phone: '0208 452 5880',
    groups: ['Office Supplies', 'Stationery']
  },
  {
    name: 'ECMS Ltd',
    address: '131 Finsbury Pavement',
    email: 'info@ecms-ltd.co.uk',
    phone: '01474 876 800',
    groups: ['Cleaners']
  }
];
