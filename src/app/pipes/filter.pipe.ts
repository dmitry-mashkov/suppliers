import { Pipe, PipeTransform } from '@angular/core';
import { toLower, filter } from 'lodash';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: Object[], property: string, search: string): any {
    return filter(items, s => (toLower(s[property]).indexOf(toLower(search)) > -1));
  }

}
