import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'prepend'
})
export class PrependPipe implements PipeTransform {

  transform(value: string, prepend: string): any {
    return `${prepend}${value}`;
  }

}
