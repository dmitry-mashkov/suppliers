import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrependPipe } from './prepend.pipe';
import { FilterPipe } from './filter.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PrependPipe, FilterPipe],
  exports: [PrependPipe, FilterPipe],
  providers: [FilterPipe]
})
export class PipesModule { }
