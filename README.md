# Office Suppliers


## Requirements

In order to develop on a local environment these programs should be installed:

* Node.js 8.*
* NPM 5.*

## Installation

Execute command `npm install`


## Usage

Execute command `npm start`. It will start the application on port 4200


*Note: production build is currently not supported*

Open the application in a browser with address `http://localhost:4200`
